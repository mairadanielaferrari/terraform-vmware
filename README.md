# How to deploy VMware vSphere Virtual Machines using Terraform

## Files

* **main.tf** --> create virtual machines

* **variables.tf** --> variables file

* **terraform.tfvars** --> update vSphere credentials and other settings

## Deployment

* Initialize providers: terraform init
* Validate configuration: terraform validate
* Update variables as needed in `terraform.tfvars`
* Preview changes:
   - PowerShell: `terraform plan --var-file=terraform.tfvars`
* Create/Update an environment:
   - PowerShell: `terraform apply --var-file=terraform.tfvars --auto-approve`
* Destroy the environment:
  - PowerShell: `terraform destroy --var-file=terraform.tfvars --auto-approve`

## Your windows template must meet the following prerequisites in order to connect using Ansible.

### Create a the Firewall Exception to allow WinRM connections

* Navigate to Computer Configuration – Windows Settings – Security Settings – Windows Firewall with Advanced Security 
* Right click on Inbound Rules and select New Rule …

![New Inbound Rule](./images/firewall-1.png)

* Select Predefined and choose Windows Remote Management from the drop down menu.

![Select Predefined](./images/firewall-2.png)

* Next, uncheck the predefined rule for the public networks.

![Uncheck public networks](./images/firewall-3.png)

* Make sure Allow the connection is selected. Click Finish.

![Allow the connection](./images/firewall-4.png)

* Check the settings.

![Check the settings](./images/firewall-5.png)


### Run the following PowerShell script (as an administrator) in order to enable the Ansible connection

```
function Enable-WinRM(){

  $url = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
  $file = "$env:temp\ConfigureRemotingForAnsible.ps1"
  (New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
  powershell.exe -ExecutionPolicy ByPass -File $file
  Enable-WSManCredSSP -Role "Server" -Force
}
 
try {
  Enable-WinRM
  exit 0;
}
Catch {
  Write-Host "An error occurred.";
  Write-Host $_;
  exit 1;
};
```
