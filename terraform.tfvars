# ======================== #
# VMware VMs configuration #
# ======================== #

vm-count = "1"
vm-name = "WSQL2016VM"
vm-template-name = "WinSrv2016SQL"
vm-cpu = 2
vm-ram = 4096
vm-guest-id = "windows9Server64Guest"
vm-disk-size = 32

# ============================ #
# VMware vSphere configuration #
# ============================ #

# VMware vCenter IP/FQDN
vsphere-vcenter = "vcenter.icssolutions.ca"

# VMware vSphere username used to deploy the infrastructure
vsphere-user = "change-me"
# VMware vSphere password used to deploy the infrastructure
vsphere-password = "change me"

# Skip the verification of the vCenter SSL certificate (true/false)
vsphere-unverified-ssl = "true"

# vSphere datacenter name where the infrastructure will be deployed
vsphere-datacenter = "Basement"

# vSphere cluster name where the infrastructure will be deployed
vsphere-cluster = "Basement-Cluster"

# vSphere Datastore used to deploy VMs
vm-datastore = "disk1"

# vSphere Network used to deploy VMs
vm-network = "VM Network"

# Linux virtual machine domain name
vm-domain = "vsphere.local"

vm-admin_password = "change-me"
vm-ipv4_gateway = "192.168.192.1"
vm-dns_server_list = ["208.67.222.222", "208.67.220.220"]
